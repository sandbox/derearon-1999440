function convert_file_max_size(size) {
  if (size == '') {
    // Drupal default max_file_size
    size = '8mb';
  }
  else {
    if (size.charAt(size.length - 1) == 'M') {
      size = size.slice(0, -1) + 'mb';
    }
    else if (size.charAt(size.length - 1) == 'K') {
      size = size.slice(0, -1) + 'kb';
    }
    else {
      size = size + 'b';
    }
  }
  return size;
}

Drupal.behaviors.plupload_uploader = function(context) {

// Declaration of array to create intances for each file field
  var uploader = new Array(); 
  // Gets an array of each file field information
  var plup = Drupal.settings.plupload_uploader.plupload;
  // File field settings  
  var validators = Drupal.settings.plupload_uploader.validators;
  
  $.each(plup, function(index, instance){
   
    // Fetching of max_file_size and implement to uploader instance setting with proper format 
    var maxFileSize = validators.filefield_validate_associate_field[index]['widget']['max_filesize_per_file'];
    maxFileSize = convert_file_max_size(maxFileSize);
    // Create plupload intsances for each file field
    uploader[index] = new plupload.Uploader({
      runtimes : instance['runtimes'],
      browse_button : instance['name'] + '_pickfiles',
      container : instance['name'] + '_container',
      max_file_size : maxFileSize,
      drop_element: instance['name'],
      multiple_queues: instance['multiple_queues'],
      url : instance['url'],
      flash_swf_url : instance['flash_swf_url'],
      silverlight_xap_url : instance['silverlight_xap_url'],
      filters : [
        {title : "All files", extensions : validators.filefield_validate_extensions[index]},
      ]
    }); 
    
    uploader[index].bind('Init', function(up, params) {
      console.log(up);
      $('#' + instance['name'] + '_filelist').html("<div>Current runtime: " + params.runtime + "</div>");
    });
    
    $('#' + instance['name'] + '_uploadfiles').click(function(e) {
      uploader[index].start();
      e.preventDefault();
    });
    
    // Upload process state
    uploader[index].bind('StateChanged', function(up) {
      if (up.state == 2) {
        //$('#' + instance['name'] + '_pickfiles').hide();
      }
      if (up.state == 1) {
        //$('#' + instance['name'] + '_pickfiles').show();
      }
    });
    
    // Removes file from queue
    $('.removeFile').live('click',function(){
      $(this).closest('tr').remove();
      uploader[index].refresh();   
    });
  
    uploader[index].init();
  
    // Uploads files if number of files and max_file_size not exceeded
    uploader[index].bind('FilesAdded', function(up, files) {
      var numberOfFiles = parseInt(validators.filefield_validate_associate_field[index]['multiple']);
      $.each(files, function(i, file) {
        var currentFiles = $('#' + instance['name'] + '_plup-list tr').length + $('#' + instance['name'] +
        '_queue-container tr').length;
        
        // Check if file is not failed before inserting in queue
        if (file.status != plupload.FAILED) { 
          
          if (numberOfFiles > 1) { // Check if number of files limited and more than one.
            if (currentFiles < numberOfFiles) {
              $('#' + instance['name'] + '_queue-container').append(
                '<tr id="' + file.id + '"><td><div>' +
                file.name + ' (' + plupload.formatSize(file.size) + ')' +
                '</div></td><td><div class="percentage"><span>0%</span></div></td></tr>'
              );
            }
            else {
              if ($('#' + instance['name'] + ' #error-container').hasClass('.file-error')) {
                $('#' + instance['name'] + ' #error-container div.file-number-error').html('Error: You can upload only ' + numberOfFiles + ' files.');
              }
              else {
                $('#' + instance['name'] + ' #error-container').addClass('file-error').append('<div class="file-number-error">Error: You can upload only ' + numberOfFiles + ' files.</div>');
              }
              up.refresh(); // Reposition Flash/Silverlight
              file.status = plupload.FAILED;
            }
          }
          else if (numberOfFiles == 1) { // Check if number of files set to unlimited.
            $('#' + instance['name'] + '_queue-container').append(
              '<tr id="' + file.id + '"><td><div>' +
              file.name + ' (' + plupload.formatSize(file.size) + ') ' +
              '</div></td><td><div class="percentage"><span>0%</span></div></td></tr>'
            );
          }
          else if (numberOfFiles == 0) { // Check if number of files set to 1.
            if (currentFiles < 1) {
              $('#' + instance['name'] + '_queue-container').append(
                '<tr id="' + file.id + '"><td><div>' +
                file.name + ' (' + plupload.formatSize(file.size) + ') ' +
                '</div></td><td><div class="percentage"><span>0%</span></div></td></tr>'
              );
            }
            else {
              if ($('#' + instance['name'] + ' #error-container').hasClass('.file-error')) {
                $('#' + instance['name'] + ' #error-container div.file-number-error').html("Error: You can upload only 1 file.");
              }
              else {
                $('#' + instance['name'] + ' #error-container').addClass('file-error').append('<div class="file-number-error">Error: You can upload only 1 file.</div>');
              }
              up.refresh(); // Reposition Flash/Silverlight
              file.status = plupload.FAILED;
            }
          }
        }
      });
      up.refresh(); // Reposition Flash/Silverlight
      uploader[index].start();
    });
    
    // Adds progress bar during uploading files
    uploader[index].bind('UploadProgress', function(up, file) {
      $('#' + file.id + " span").css({'width' : up.total.percent + '%', 'background-color' : 'green'}).html(up.total.percent + '%');
    });

    uploader[index].bind('Error', function(up, err) {
      $('#' + instance['name'] + ' #error-container').append("<div>Error: " +
        "Message: " + err.message +
          (err.file ? ", File: " + err.file.name : "") +
        "</div>"
      );
      up.refresh(); // Reposition Flash/Silverlight
    });

    uploader[index].bind('FileUploaded', function(up, file, response) {
      // Here we get response from the server side with uploaded file information.
      if (response.status == 200 || up.runtime == 'flash' || up.runtime == 'html4') {
        var fileResponse = Drupal.parseJson(response.response);
        var delta = $('#' + instance['name'] + '_plup-list tr').length;
        var field_name = $('#' + file.id).closest('.plupload-uploader-widget').attr('id');
        $('#' + instance['name'] + '_queue-container #' + file.id).remove(); // Remove uploaded file from queue
        $('#' + instance['name'] + '_plup-list').append(
          '<tr id="' + file.id + '" class="' + (delta % 2 == 0 ? 'even': 'odd') + '"><td><label for="' + field_name + '[' + delta + '][fid]">' + (delta + 1) + '</label></td>' +
          '<td><div class="form-item">' +
          fileResponse.filename + ' (' + plupload.formatSize(file.size) + ') <b></b>' + '</div></td>' +
          '<td><div class="form-buttons">' +
          '<a href="#deleteContent" id="' + file.id + '" class="removeFile button small grey delete-area-link"><span>x</span></a>' +
          '</div></td></tr>'
        );
        $('#' + instance['name'] + '_plup-list tr#' + file.id + ' div').append('<input type="hidden" name="' +
          field_name + '[' + delta + '][fid]" value="' + fileResponse.fid + '" />');
        //$('#' + file.id + " b").html("100%"); 
      }
      else {
        // File uploading errors
        var errorMessage = Drupal.parseJson(response.response);
        $('#' + instance['name'] + '_filelist').append("<div>Error: " + errorMessage.error.code +
          ", Message: " + errorMessage.error.message +
          "</div>"
        );
        alert("Error: " + errorMessage.error.code + ", Message: " + errorMessage.error.message);
        return;
      }
    });
  });
};
